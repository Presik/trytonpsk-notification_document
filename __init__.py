# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import document

def register():
    Pool.register(
        document.NotificationDocument,
        document.NotificationDocumentType,
        module='notification_document', type_='model')
