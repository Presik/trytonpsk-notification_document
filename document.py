# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, timedelta
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class NotificationDocumentType(ModelSQL, ModelView):
    'Notification Document Type'
    __name__ = 'notification.document.type'
    name = fields.Char('Name', select=True, required=True)
    active = fields.Boolean('Active')
    expire_time = fields.Integer('Expire Time', required=True, help="In days")
    prenotification_days = fields.Integer('Prenotification Days',
        required=True, help='In days')
    model = fields.Many2One('ir.model', 'Model', required=True)
    email_template = fields.Many2One('email.template', 'Template')

    @classmethod
    def __setup__(cls):
        super(NotificationDocumentType, cls).__setup__()

    @staticmethod
    def default_active():
        return True


class NotificationDocument(ModelSQL, ModelView):
    'Notification Document'
    __name__ = 'notification.document'
    type = fields.Many2One('notification.document.type', 'Type', required=True)
    notes = fields.Char('Notes', select=True)
    register_date = fields.Date('Register Date', required=True)
    expire_date = fields.Date('Expire Date', required=True)
    expire_days = fields.Function(fields.Integer('Days'), 'get_expire_days')
    origin = fields.Reference('Origin', selection='get_origin')

    @classmethod
    def __setup__(cls):
        super(NotificationDocument, cls).__setup__()

    def get_expire_days(self, name=None):
        if self.type and self.expire_date:
            today = date.today()
            return (self.expire_date - today).days

    @fields.depends('register_date', 'type')
    def on_change_with_expire_date(self, name=None):
        if self.type and self.register_date:
            return (self.register_date + timedelta(self.type.expire_time))

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return []

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @classmethod
    def send_mail_notification(cls):
        pool = Pool()
        today = date.today()
        Template = pool.get('email.template')
        DocumentType = pool.get('notification.document.type')

        types = DocumentType.search([
            ('email_template', '!=', None)
        ])

        for t in types:
            documents = cls.search([
                ('type.email_template', '=', t.email_template.id)
            ])

            records_ = []
            for doc in documents:
                delta = (doc.expire_date - today).days
                if doc.origin:
                    if hasattr(doc.origin, 'active') and not doc.origin.active:
                        continue
                    if delta < doc.type.prenotification_days:
                        records_.append(doc)

            if records_:
                Template.send(t.email_template, records_)
